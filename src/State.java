/**
 * State interface contain method used in clock state.
 * @author Prin Angkunanuwat
 *
 */
public interface State {
	/** Perform an action when the set button is pressed. */
	public void performSet();
	/** Perform an action when the plus button is pressed. */
	public void performPlus();
	/** Perform an action when the minus button is pressed. */
	public void performMinus();
	/** Update the time show in ui. */
	public void updateTime();
	/** Perform action before leave the state. */
	public void leaveState();
}
