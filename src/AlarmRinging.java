import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;
/**
 * Alarm ringing state that play sound.
 * @author Prin Angkunanuwat
 *
 */
public class AlarmRinging implements State {
	private static DigitalClock clock;
	private AudioClip clip;
	/**
	 * Get wav file and set up atribute.
	 * @param clock is digital clock.
	 */
	public AlarmRinging(DigitalClock clock) {
		this.clock = clock;
		URL url = getClass().getResource("audio/SIREN.WAV");
		clip = Applet.newAudioClip(url);
	}
	/**
	 * Stop the sound and change the state.
	 */
	@Override
	public void performSet() {
		turnOff();

	}
	/**
	 * Play the sound continually.
	 */
	@Override
	public void updateTime() {
		clip.play();

	}
	/** This method does nothing. */
	@Override
	public void leaveState() {}
	/**
	 * Stop the sound and change the state.
	 */
	@Override
	public void performPlus() {
		turnOff();

	}
	/**
	 * Stop the sound and change the state.
	 */
	@Override
	public void performMinus() {
		turnOff();

	}
	/**
	 * Stop the sound and change the state.
	 */
	public void turnOff() {
		clip.stop();
		clock.setState(clock.DisplayTime);
	}

}
