import java.util.Date;
/**
 * A state that show the current time.
 * @author Prin Angkunanuwat
 *
 */
public class DisplayTime implements State {
	private DigitalClock clock;
	private Date time = new Date();
	/**
	 * Setup the clock. 
	 * @param clock is digital clock.
	 */
	public DisplayTime(DigitalClock clock) {
		this.clock = clock;
	}
	/**
	 * Change state to setting Alarm.
	 */
	@Override
	public void performSet() {
		clock.setState(clock.SettingAlarm);
	}
	/**
	 * Update the time to current time.
	 * Also check the alarm time.
	 */
	@Override
	public void updateTime() {
		time.setTime(System.currentTimeMillis());
		clock.setHour(time.getHours());
		clock.setMinute(time.getMinutes());
		clock.setSecond(time.getSeconds());

		if (clock.getAlarm(0) == clock.getHour()
				&& clock.getAlarm(1) == clock.getMinute()
				&& clock.getAlarm(2) == clock.getSecond()) {
			clock.setState(clock.AlarmRinging);
		}
	}
	/** This method does nothing. */
	@Override
	public void leaveState() {}
	/** This method does nothing. */
	@Override
	public void performPlus() {}
	/** This method does nothing. */
	@Override
	public void performMinus() {}

}
