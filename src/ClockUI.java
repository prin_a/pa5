import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * UI for digital clock.
 * @author Prin Angkunanuwat
 *
 */
public class ClockUI extends JFrame {
	private JLabel h1;
	private JLabel h2;
	private JLabel m1;
	private JLabel m2;
	private JLabel s1;
	private JLabel s2;
	private JButton set;
	private JButton plus;
	private JButton minus;

	private DigitalClock clock;
	/**
	 * Create ui.
	 */
	public ClockUI() {
		super("Digital Clock");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(210, 125);
		this.setResizable(false);
		this.initComponents();
		this.setVisible(true);
		clock = new DigitalClock();

	}
	/**
	 * Init component of this ui.
	 */
	private void initComponents() {
		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout();
		contents.setLayout(layout);
		contents.setBackground(Color.BLACK);

		Class clazz = this.getClass();
		URL[] alarm = new URL[10];
		ImageIcon[] icon = new ImageIcon[10];
		for (int i = 0; i < icon.length; i++) {
			alarm[i] = clazz.getResource("images/" + i + ".gif");
			icon[i] = new ImageIcon(alarm[i]);
		}
		

		JLabel colon1 = new JLabel(" : ");
		JLabel colon2 = new JLabel(" : ");
		h1 = new JLabel(icon[0]);
		h2 = new JLabel(icon[0]);
		m1 = new JLabel(icon[0]);
		m2 = new JLabel(icon[0]);
		s1 = new JLabel(icon[0]);
		s2 = new JLabel(icon[0]);

		set = new JButton("Set");
		plus = new JButton("+");
		minus = new JButton("-");

		contents.add(h1);
		contents.add(h2);
		contents.add(colon1);
		contents.add(m1);
		contents.add(m2);
		contents.add(colon2);
		contents.add(s1);
		contents.add(s2);

		contents.add(set);
		contents.add(minus);
		contents.add(plus);

		set.addActionListener(new SetListener());
		plus.addActionListener(new PlusListener());
		minus.addActionListener(new MinusListener());

		int delay = 500; // milliseconds
		ActionListener task = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent evt) {
				clock.updateTime();
				int hours = clock.getHour();
				int minutes = clock.getMinute();
				int seconds = clock.getSecond();
				h1.setIcon(icon[hours / 10]);
				h2.setIcon(icon[hours % 10]);
				m1.setIcon(icon[minutes / 10]);
				m2.setIcon(icon[minutes % 10]);
				s1.setIcon(icon[seconds / 10]);
				s2.setIcon(icon[seconds % 10]);
			}
		};
		javax.swing.Timer timer = new javax.swing.Timer(delay, task);
		timer.start();

	}
	/**
	 * Perform action when the set button is pressed.
	 *
	 */
	class SetListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			clock.handleSetKey();
		}
	}

	/**
	 * Perform action when the plus button is pressed.
	 *
	 */
	class PlusListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			clock.handlePlusKey();
		}
	}

	/**
	 * Perform action when the minus button is pressed.
	 *
	 */
	class MinusListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			clock.handleMinusKey();
		}
	}
}
