
/**
 * Digital clock contain the current time.
 * @author Prin Angkunanuwat
 *
 */
public class DigitalClock {
	public final State DisplayTime = new DisplayTime(this);
	public final State SettingAlarm = new SettingAlarm(this);
	public final State AlarmRinging = new AlarmRinging(this);

	private State state;
	private int hour;
	private int minute;
	private int second;

	private int[] alarmTime;
	/**
	 * Construct a clock.
	 */
	public DigitalClock() {
		state = DisplayTime;
		alarmTime = new int[3];


	}

	/** delegate behavior to state objects */
	public void updateTime() {
		state.updateTime();
	}
	/** delegate behavior to state objects */
	public void handleSetKey() {
		state.performSet();
	}
	/** delegate behavior to state objects */
	public void handlePlusKey() {
		state.performPlus();
	}
	/** delegate behavior to state objects */
	public void handleMinusKey() {
		state.performMinus();
	}
	/**
	 * Change current state to new state.
	 * @param newState is new state for clock.
	 */
	public void setState(State newState) {
		if (this.state != newState)
			state.leaveState();
		state = newState;
	}
	/**
	 * Set method for alarm time.
	 * @param time to be set.
	 * @param index of an array. 0 is hour, 1 is minute and 2 is second.
	 */
	public void setAlarm(int time, int index) {
		alarmTime[index] = time;
	}
	/**
	 * Get method for alarm time.
	 * @param index of an array. 0 is hour, 1 is minute and 2 is second.
	 * @return time.
	 */
	public int getAlarm(int index) {
		return alarmTime[index];
	}
	/**
	 * Set current hour to new hour.
	 * @param hours is new hour.
	 */
	public void setHour(int hours) {
		this.hour = hours;
	}
	/**
	 * Set current minute to new minute.
	 * @param minutes is new minute.
	 */
	public void setMinute(int minutes) {
		this.minute = minutes;
	}
	/**
	 * Set current second to new second.
	 * @param seconds is new second.
	 */
	public void setSecond(int seconds) {
		this.second = seconds;
	}
	/**
	 * Get hour from the clock.
	 * @return hour
	 */
	public int getHour() {
		return hour;
	}
	/**
	 * Get minute from the clock.
	 * @return minute
	 */
	public int getMinute() {
		return minute;
	}
	/**
	 * Get second from the clock.
	 * @return second
	 */
	public int getSecond() {
		return second;
	}
}