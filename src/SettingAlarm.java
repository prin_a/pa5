/**
 * Setting alarm time for the clock.
 * @author Prin Angkunanuwat
 *
 */
public class SettingAlarm implements State {
	private DigitalClock clock;
	private SettingState HOUR = new HOUR();
	private SettingState MINUTE = new MINUTE();
	private SettingState SECOND = new SECOND();
	private SettingState state = HOUR;
	/**
	 * Set current setting state to new state.
	 * @param newState is new setting alarm state.
	 */
	public void setState(SettingState newState) {
		state = newState;
	}
	/**
	 * Setup the clock. 
	 * @param clock is digital clock.
	 */
	public SettingAlarm(DigitalClock clock) {
		this.clock = clock;
	}
	/** delegate behavior to setting alarm state objects. */
	@Override
	public void performSet() {
		state.performSet();

	}
	/** This method does nothing. */
	@Override
	public void updateTime() {}
	/** Set setting state to HOUR before exit this state. */
	@Override
	public void leaveState() {
		setState(HOUR);
	}
	/** delegate behavior to setting alarm state objects. */
	@Override
	public void performPlus() {
		state.plusButton();

	}
	/** delegate behavior to setting alarm state objects. */
	@Override
	public void performMinus() {
		state.minusButton();

	}
	/**
	 * Interface for setting state.
	 *
	 */
	public interface SettingState {
		/** Perform an action when the plus button is pressed. */
		public void plusButton();
		/** Perform an action when the minus button is pressed. */
		public void minusButton();
		/** Perform an action when the set button is pressed. */
		public void performSet();
	}
	/**
	 * HOUR state that setting hour in alarm time.
	 */
	class HOUR implements SettingState {
		int hours;
		/**
		 * Increase hour by one.
		 */
		@Override
		public void plusButton() {
			hours = clock.getHour();
			if (hours >= 23)
				clock.setHour(0);
			else
				clock.setHour(hours + 1);

		}
		/**
		 * Decrease hour by one.
		 */
		@Override
		public void minusButton() {
			hours = clock.getHour();
			if (hours <= 0)
				clock.setHour(23);
			else
				clock.setHour(hours - 1);

		}
		/**
		 * Change current setting state to minute.
		 */
		@Override
		public void performSet() {
			state = MINUTE;
			clock.setAlarm(clock.getHour(),0);

		}
	}
	/**
	 * MINUTE state that setting minute in alarm time.
	 */
	class MINUTE implements SettingState {
		int minutes;
		/**
		 * Increase minute by one.
		 */
		@Override
		public void plusButton() {
			minutes = clock.getMinute();
			if (minutes >= 59)
				clock.setMinute(0);
			else
				clock.setMinute(minutes + 1);

		}
		/**
		 * Decrease minute by one.
		 */
		@Override
		public void minusButton() {
			minutes = clock.getMinute();
			if (minutes <= 0)
				clock.setMinute(59);
			else
				clock.setMinute(minutes - 1);

		}
		/**
		 * Change current setting state to second.
		 */
		@Override
		public void performSet() {
			state = SECOND;
			clock.setAlarm(clock.getMinute(),1);

		}
	}
	/**
	 * SECOND state that setting second in alarm time.
	 */
	class SECOND implements SettingState {
		int seconds;
		/**
		 * Increse second by one.
		 */
		@Override
		public void plusButton() {
			seconds = clock.getSecond();
			if (seconds >= 59)
				clock.setSecond(0);
			else
				clock.setSecond(seconds + 1);

		}
		/**
		 * Decrease second by one.
		 */
		@Override
		public void minusButton() {
			seconds = clock.getSecond();
			if (seconds <= 0)
				clock.setSecond(59);
			else
				clock.setSecond(seconds - 1);

		}
		/**
		 * Exit setting alarm state.
		 */
		@Override
		public void performSet() {
			clock.setState(clock.DisplayTime);
			clock.setAlarm(clock.getSecond(),2);

		}
	}

}
