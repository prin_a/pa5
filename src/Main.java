/**
 * Main class to run the clock.
 * @author Prin Angkunanuwat
 *
 */
public class Main {
	/**
	 * Main method create instance of clock.
	 * @param args is not used.
	 */
	public static void main(String[] args) {
		ClockUI c = new ClockUI();
	}
}
